use futures::compat::Future01CompatExt;
use graphql_client::web::ClientError;
use graphql_client::{GraphQLQuery, Response};
use serde::Serialize;

pub mod login;
pub mod register;

pub async fn execute_query<Q>(
    query: Q,
    variables: Q::Variables,
) -> Result<Response<Q::ResponseData>, ClientError>
where
    Q: GraphQLQuery + 'static,
{
    let client = graphql_client::web::Client::new("http://127.0.0.1:8080/graphql");
    client.call(query, variables).compat().await
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ClientData {
    pub ip_address: String,
}

// wait until async is stabilized
//#[wasm_bindgen_test]
//fn query_test() -> Result<(), ClientError> {
//    use crate::graphql::register::{Register, RegisterRequestVariables};
//    use chrono::NaiveDate;
//    let result = execute_query(
//        Register {},
//        RegisterRequestVariables {
//            birth_date: NaiveDate::from_ymd(1990, 1, 1),
//            address: "Piazza La Bomba e Scappa".to_string(),
//            email: "test@email.com".to_string(),
//            password: "strongPwD!".to_string(),
//            primary_name: "Gino".to_string(),
//            secondary_name: "".to_string(),
//            surname: "Pino".to_string(),
//        },
//    )
//    .data
//    .unwrap();
//
//    assert!(result.register.success);
//    Ok(())
//}
