pub const OPERATION_NAME: &str = "login";
pub const QUERY: &str = include_str!("queries/login.graphql");
use crate::graphql::ClientData;
use graphql_client::{GraphQLQuery, QueryBody};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct LoginRequestVariables {
    pub email: String,
    pub password: String,
    pub client_data: ClientData,
}
#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct LoginData {
    pub login_data: LoginRequestVariables,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
#[serde(untagged)]
pub enum LoginResponseUnion {
    LoginOk(LoginOk),
    LoginError(LoginError),
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct LoginOk {
    pub token: String,
    pub refresh_token: String,
    pub tracking_id: Uuid,
}

#[derive(Deserialize, Debug)]
pub struct LoginError {
    pub error: String,
}

#[derive(Deserialize, Debug)]
pub struct ResponseData {
    pub login: LoginResponseUnion,
}
pub struct Login;

impl GraphQLQuery for Login {
    type Variables = LoginData;
    type ResponseData = ResponseData;
    fn build_query(variables: Self::Variables) -> QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: QUERY,
            operation_name: OPERATION_NAME,
        }
    }
}
