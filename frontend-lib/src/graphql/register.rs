pub const OPERATION_NAME: &str = "register";
pub const QUERY: &str = include_str!("queries/register.graphql");
use crate::graphql::ClientData;
use graphql_client::{GraphQLQuery, QueryBody};
use serde::{Deserialize, Serialize};

type Boolean = bool;
#[doc = "NaiveDate YYYY-mm-dd"]
type NaiveDate = chrono::NaiveDate;

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterRequestVariables {
    pub birth_date: NaiveDate,
    pub address: String,
    pub email: String,
    pub password: String,
    pub primary_name: String,
    pub secondary_name: String,
    pub surname: String,
    pub client_data: ClientData,
}
#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterData {
    pub reg_data: RegisterRequestVariables,
}

#[derive(Deserialize, Debug)]
pub struct RegisterResponseData {
    pub success: Boolean,
}

#[derive(Deserialize, Debug)]
pub struct ResponseData {
    pub register: RegisterResponseData,
}
pub struct Register;

impl GraphQLQuery for Register {
    type Variables = RegisterData;
    type ResponseData = ResponseData;
    fn build_query(variables: Self::Variables) -> QueryBody<Self::Variables> {
        graphql_client::QueryBody {
            variables,
            query: QUERY,
            operation_name: OPERATION_NAME,
        }
    }
}
