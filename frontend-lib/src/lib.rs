/// The global session cookie name
pub mod graphql;

pub const SESSION_COOKIE: &str = "sessionToken";
