use crate::entity::form::{register::ValidForm as EntityValidForm, FormField};
use indexmap::IndexMap;
use serde::Serialize;

#[derive(Serialize)]
pub struct ValidForm<'a> {
    pub value_map: IndexMap<&'a str, &'a str>,
}

impl<'a> ValidForm<'a> {
    pub fn new(form: &'a EntityValidForm) -> Self {
        ValidForm {
            value_map: form
                .iter_keys_and_fields()
                .map(|(key, field)| (*key, field.value()))
                .collect(),
        }
    }
}

// ====== ====== TESTS ====== ======

// see `src/code/encoder/comments` for example how to test encoder
