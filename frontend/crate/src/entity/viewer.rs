use crate::{
    entity::{Avatar, Profile, Username},
    storage,
};
use frontend_lib::graphql::login;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Viewer {
    pub profile: Profile,
    pub auth_token: String,
    pub refresh_token: String,
    pub tracking_id: Uuid,
}

impl Viewer {
    pub const fn username(&self) -> &Username {
        &self.profile.username
    }

    pub const fn avatar(&self) -> &Avatar {
        &self.profile.avatar
    }

    pub fn store(&self) {
        storage::store_viewer(self);
    }
}

impl From<login::LoginOk> for Viewer {
    fn from(data: login::LoginOk) -> Self {
        Self {
            profile: Profile {
                bio: None,
                avatar: Avatar::new(None as Option<&str>),
                username: Default::default(),
            },
            auth_token: data.token,
            refresh_token: data.refresh_token,
            tracking_id: data.tracking_id,
        }
    }
}
