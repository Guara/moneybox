use crate::{
    coder::encoder::form::register::ValidForm as ValidFormEncoder,
    entity::form::{self, FormField},
};
use chrono::NaiveDate;
use strum::IntoEnumIterator;
use strum_macros::EnumIter;
use unicode_segmentation::UnicodeSegmentation;

pub const EMAIL_KEY: &str = "email";
pub const PASSWORD_KEY: &str = "password";
pub const BIRTH_DATE_KEY: &str = "birt_date";
pub const ADDRESS_KEY: &str = "address";
pub const PRIMARY_NAME_KEY: &str = "primary_name";
pub const SECONDARY_NAME_KEY: &str = "secondary_name";
pub const SURNAME_KEY: &str = "surname";

// ------ Form ------

pub type Form = form::Form<Field>;

impl Default for Form {
    fn default() -> Self {
        Self::new(Field::iter())
    }
}

// ------ ValidForm ------

pub type ValidForm = form::ValidForm<Field>;

impl ValidForm {
    pub fn to_encoder(&self) -> ValidFormEncoder {
        ValidFormEncoder::new(self)
    }
}

// ------ Problem ------

pub type Problem = form::Problem;

// ------ Field ------

#[derive(Clone, EnumIter)]
pub enum Field {
    Email(String),
    Password(String),
    BirthDate(String),
    Address(String),
    PrimaryName(String),
    SecondaryName(String),
    Surname(String),
}

impl FormField for Field {
    fn value(&self) -> &str {
        use Field::*;
        match self {
            Email(value) | Password(value) | Address(value) | PrimaryName(value)
            | BirthDate(value) | SecondaryName(value) | Surname(value) => value,
        }
    }

    fn value_mut(&mut self) -> &mut String {
        use Field::*;
        match self {
            Email(value) | Password(value) | Address(value) | BirthDate(value)
            | PrimaryName(value) | SecondaryName(value) | Surname(value) => value,
        }
    }

    fn key(&self) -> &'static str {
        use Field::*;
        match self {
            Email(_) => EMAIL_KEY,
            Password(_) => PASSWORD_KEY,
            BirthDate(_) => BIRTH_DATE_KEY,
            Address(_) => ADDRESS_KEY,
            PrimaryName(_) => PRIMARY_NAME_KEY,
            SecondaryName(_) => SECONDARY_NAME_KEY,
            Surname(_) => SURNAME_KEY,
        }
    }

    fn validate(&self) -> Option<form::Problem> {
        use Field::*;
        match self {
            Email(value) => should_not_be_empty(self.key(), value),
            Password(value) => match value.graphemes(true).count() {
                0 => Some(form::Problem::new_invalid_field(
                    self.key(),
                    "password can't be blank",
                )),
                // @TODO: use exclusive range pattern once stabilized
                // https://github.com/rust-lang/rust/issues/37854
                i @ 1..=form::MIN_PASSWORD_LENGTH if i < form::MIN_PASSWORD_LENGTH => {
                    Some(form::Problem::new_invalid_field(
                        self.key(),
                        format!(
                            "password is too short (minimum is {} characters)",
                            form::MIN_PASSWORD_LENGTH
                        ),
                    ))
                }
                _ => None,
            },
            BirthDate(value) => NaiveDate::parse_from_str(value, "%d/%m/%Y")
                .ok()
                .map_or_else(
                    || {
                        Some(form::Problem::new_invalid_field(
                            self.key(),
                            "date should be formatted as dd/mm/YYYY",
                        ))
                    },
                    |_| None,
                ),
            Address(value) => should_not_be_empty(self.key(), value),
            PrimaryName(value) => should_not_be_empty(self.key(), value),
            SecondaryName(_) => None,
            Surname(value) => should_not_be_empty(self.key(), value),
        }
    }
}

// ====== ====== TESTS ====== ======

#[cfg(test)]
pub mod tests {
    use super::*;
    use wasm_bindgen_test::*;

    #[wasm_bindgen_test]
    fn valid_form_test() {
        // ====== ARRANGE ======
        let mut form = Form::default();
        form.upsert_field(Field::Email("john@example.com".into()));
        form.upsert_field(Field::Password("12345678".into()));

        // ====== ACT ======
        let result = form.trim_fields().validate();

        // ====== ASSERT ======
        assert!(result.is_ok());
    }

    #[wasm_bindgen_test]
    fn invalid_form_test() {
        // ====== ARRANGE ======
        let form = Form::default();

        // ====== ACT ======
        let result = form.trim_fields().validate();

        // ====== ASSERT ======
        assert!(if let Err(problems) = result {
            vec![
                "username can't be blank",
                "email can't be blank",
                "password can't be blank",
            ] == problems
                .iter()
                .map(form::Problem::message)
                .collect::<Vec<_>>()
        } else {
            false
        });
    }

    #[wasm_bindgen_test]
    fn short_password_test() {
        // ====== ARRANGE ======
        let mut form = Form::default();
        form.upsert_field(Field::Password("1234567".into()));

        // ====== ACT ======
        let result = form.trim_fields().validate();

        // ====== ASSERT ======
        assert!(if let Err(problems) = result {
            problems.iter().any(|problem| {
                problem.message() == "password is too short (minimum is 8 characters)"
            })
        } else {
            false
        });
    }
}

fn should_not_be_empty(key: &'static str, value: &str) -> Option<Problem> {
    if value.is_empty() {
        Some(form::Problem::new_invalid_field(
            key,
            format!("{} can't be blank", key),
        ))
    } else {
        None
    }
}
