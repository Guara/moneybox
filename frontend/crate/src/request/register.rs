use crate::entity::form::register::{
    ADDRESS_KEY, BIRTH_DATE_KEY, EMAIL_KEY, PASSWORD_KEY, PRIMARY_NAME_KEY, SECONDARY_NAME_KEY,
    SURNAME_KEY,
};
use crate::{
    coder,
    entity::form::register::{Problem, ValidForm},
    request,
};
use chrono::NaiveDate;
use frontend_lib::graphql;
use frontend_lib::graphql::register::{RegisterData, RegisterRequestVariables};
use frontend_lib::graphql::ClientData;

pub async fn register<Ms: 'static>(
    valid_form: ValidForm,
    f: fn(Result<bool, Vec<Problem>>) -> Ms,
) -> Result<Ms, Ms> {
    let variables = to_register_data(&valid_form.to_encoder());
    graphql::execute_query(graphql::register::Register {}, variables)
        .await
        .map(move |response| {
            if let Some(data) = response.data {
                f(Ok(data.register.success))
            } else if let Some(err) = response.errors {
                f(Err(request::graphql_errors_into_problems(Some(err))))
            } else {
                f(Err(request::graphql_errors_into_problems(None)))
            }
        })
        .map_err(move |err| f(Err(vec![request::client_error_into_problem(err)])))
}

// Not implement the trait, this function should be called only from the above register fn that recived a validated form
fn to_register_data(valid_form: &coder::encoder::form::register::ValidForm) -> RegisterData {
    RegisterData {
        reg_data: RegisterRequestVariables {
            birth_date: NaiveDate::parse_from_str(
                valid_form.value_map.get(BIRTH_DATE_KEY).unwrap(),
                "%d/%m/%Y",
            )
            .unwrap(),
            address: valid_form.value_map.get(ADDRESS_KEY).unwrap().to_string(),
            email: valid_form.value_map.get(EMAIL_KEY).unwrap().to_string(),
            password: valid_form.value_map.get(PASSWORD_KEY).unwrap().to_string(),
            primary_name: valid_form
                .value_map
                .get(PRIMARY_NAME_KEY)
                .unwrap()
                .to_string(),
            secondary_name: valid_form
                .value_map
                .get(SECONDARY_NAME_KEY)
                .unwrap()
                .to_string(),
            surname: valid_form.value_map.get(SURNAME_KEY).unwrap().to_string(),
            client_data: ClientData {
                ip_address: "0.0.0.0".to_string(),
            },
        },
    }
}
