use crate::entity::form::login::{EMAIL_KEY, PASSWORD_KEY};
use crate::{
    coder,
    coder::decoder,
    entity::{
        form::login::{Problem, ValidForm},
        Viewer,
    },
    request,
};
use frontend_lib::graphql;
use frontend_lib::graphql::login::{LoginData, LoginRequestVariables, LoginResponseUnion};
use frontend_lib::graphql::ClientData;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct RootDecoder {
    user: decoder::Viewer,
}

pub async fn login<Ms: 'static>(
    valid_form: ValidForm,
    f: fn(Result<Viewer, Vec<Problem>>) -> Ms,
) -> Result<Ms, Ms> {
    let variables = to_login_data(&valid_form.to_encoder());
    graphql::execute_query(graphql::login::Login {}, variables)
        .await
        .map(move |response| {
            if let Some(data) = response.data {
                match data.login {
                    LoginResponseUnion::LoginOk(data) => f(Ok(data.into())),
                    LoginResponseUnion::LoginError(err) => {
                        f(Err(vec![request::pretty_error_into_problem(err.error)]))
                    }
                }
            } else if let Some(err) = response.errors {
                f(Err(request::graphql_errors_into_problems(Some(err))))
            } else {
                f(Err(request::graphql_errors_into_problems(None)))
            }
        })
        .map_err(move |err| f(Err(vec![request::client_error_into_problem(err)])))
}

// Not implement the trait, this function should be called only from the above login fn that recived a validated form
fn to_login_data(valid_form: &coder::encoder::form::login::ValidForm) -> LoginData {
    LoginData {
        login_data: LoginRequestVariables {
            email: valid_form.value_map.get(EMAIL_KEY).unwrap().to_string(),
            password: valid_form.value_map.get(PASSWORD_KEY).unwrap().to_string(),
            client_data: ClientData {
                ip_address: "0.0.0.0".to_string(),
            },
        },
    }
}
