use super::context::Context;
use crate::graphql;
use crate::graphql::types::login::{LoginError, LoginOk, LoginResponse};
use crate::graphql::types::register::RegisterResponse;
use backend_lib::application::policies;
use backend_lib::application::policies::Policy;
use backend_lib::db::connection::get_connection;
use backend_lib::errors::BackendError;
use juniper::FieldResult;
use uuid::Uuid;

pub struct QueryRoot;

#[juniper::object(Context = Context)]
impl QueryRoot {
    fn me(context: &Context) -> FieldResult<&graphql::types::login::User> {
        if let Some(user) = &context.user {
            Ok(user)
        } else {
            Err(BackendError::Unauthorized.into())
        }
    }
}

pub struct MutationRoot;

#[juniper::object(Context = Context)]
impl MutationRoot {
    fn register(
        req_data: graphql::types::register::RegisterData,
    ) -> FieldResult<graphql::types::register::RegisterResponse> {
        policies::user::CreateUserPolicy::handle_input(req_data.into(), &get_connection())
            .map(|_| RegisterResponse::ok())
            .map_err(Into::into)
    }

    fn login(
        login_data: graphql::types::login::LoginData,
    ) -> FieldResult<graphql::types::login::LoginResponse> {
        let email = login_data.email.clone();
        match policies::user::LoginUserPolicy::handle_input(login_data.into(), &get_connection()) {
            Ok((token, refresh_token)) => {
                Ok(LoginResponse::Ok(LoginOk {
                    token,
                    refresh_token,
                    tracking_id: Uuid::new_v4(), //should be fetched in the db, or maybe returned from the policy in some way...
                }))
            }
            Err(err) => match err {
                BackendError::UserNotFound | BackendError::Unauthorized => {
                    Ok(LoginResponse::Error(LoginError {
                        error: "username and password doesn't match".to_string(),
                    }))
                }
                _ => Err(err.into()),
            },
        }
    }

    fn new_piggybank(
        context: &Context,
        piggybank_data: graphql::types::NewPiggyBankData,
    ) -> FieldResult<graphql::types::NewPiggyBankResponse> {
        if let Some(user) = &context.user {
            Ok(policies::piggybank::CreatePiggyBankPolicy::handle_input(
                piggybank_data.into(),
                &get_connection(),
            )
            .map(Into::into)?)
        } else {
            Err(BackendError::Unauthorized.into())
        }
    }
}
