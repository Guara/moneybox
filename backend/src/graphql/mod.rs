pub mod context;
pub mod schema;
pub mod types;

use self::schema::{MutationRoot, QueryRoot};
use juniper::RootNode;

pub type Schema = RootNode<'static, QueryRoot, MutationRoot>;

pub fn create_schema() -> Schema {
    Schema::new(QueryRoot {}, MutationRoot {})
}
