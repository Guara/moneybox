//use crate::config::Config;

use crate::graphql::types::login::User;

pub struct Context {
    //    pub config: Config,
    pub user: Option<User>,
}

impl Default for Context {
    fn default() -> Self {
        Self {
            //            config: Config {
            //                listen: "".to_owned(),
            //            },
            user: None,
        }
    }
}

impl juniper::Context for Context {}
