use crate::graphql::context::Context;
use crate::graphql::types::{ClientData, GqlDecimal, PiggyBank};
use backend_lib::application::policies;
use backend_lib::application::policies::user::LoginUserPolicy;
use backend_lib::db::connection::get_connection;
use backend_lib::errors::BackendResult;
use backend_lib::read_model;
use chrono::{DateTime, NaiveDate, Utc};
use juniper::FieldResult;
use serde::Deserialize;
use uuid::Uuid;

#[derive(GraphQLInputObject, Clone)]
pub struct LoginData {
    pub email: String,
    pub password: String,
    pub client_data: ClientData,
}

impl Into<LoginUserPolicy> for LoginData {
    fn into(self) -> LoginUserPolicy {
        LoginUserPolicy {
            client_data: self.client_data.clone().into(),
            login_data: self.into(),
        }
    }
}

impl Into<policies::user::LoginData> for LoginData {
    fn into(self) -> policies::user::LoginData {
        policies::user::LoginData {
            email: self.email,
            password: self.password,
        }
    }
}

#[derive(Deserialize, GraphQLObject, Debug)]
#[serde(rename_all = "camelCase")]
pub struct LoginOk {
    pub token: String,
    pub refresh_token: String,
    pub tracking_id: Uuid,
}

#[derive(Deserialize, GraphQLObject, Debug)]
pub struct LoginError {
    pub error: String,
}

#[derive(Deserialize, Debug)]
#[serde(untagged)]
pub enum LoginResponse {
    Ok(LoginOk),
    Error(LoginError),
}

graphql_union!(LoginResponse: () as "LoginResponse" where Scalar = <S> |&self| {
    instance_resolvers: |_| {
        &LoginOk => match *self { LoginResponse::Ok(ref h) => Some(h), _ => None },
        &LoginError => match *self { LoginResponse::Error(ref d) => Some(d), _ => None },
    }
});

#[derive(Debug)]
pub struct User {
    pub user: BackendResult<read_model::User>,
    pub permissions: Vec<String>,
}

#[juniper::object(Context = Context)]
impl User {
    fn email(&self) -> FieldResult<&str> {
        Ok(&(self.user.as_ref()?.email))
    }

    fn primary_name(&self) -> FieldResult<&str> {
        Ok(&(self.user.as_ref()?.primary_name))
    }

    fn secondary_name(&self) -> FieldResult<&str> {
        Ok(&(self.user.as_ref()?.secondary_name))
    }

    fn surname(&self) -> FieldResult<&str> {
        Ok(&(self.user.as_ref()?.surname))
    }

    fn birth_date(&self) -> FieldResult<&NaiveDate> {
        Ok(&(self.user.as_ref()?.birth_date))
    }

    fn address(&self) -> FieldResult<&str> {
        Ok(&(self.user.as_ref()?.address))
    }

    fn registration_date(&self) -> FieldResult<&DateTime<Utc>> {
        Ok(&(self.user.as_ref()?.registration_date))
    }

    fn ibans(&self) -> FieldResult<Vec<&str>> {
        Ok(self
            .user
            .as_ref()?
            .ibans
            .iter()
            .map(AsRef::as_ref)
            .collect())
    }

    fn credit_cards(&self) -> FieldResult<Vec<&str>> {
        Ok(self
            .user
            .as_ref()?
            .credit_cards
            .iter()
            .map(AsRef::as_ref)
            .collect())
    }

    fn fund(&self) -> FieldResult<GqlDecimal> {
        Ok(GqlDecimal(self.user.as_ref()?.fund.clone()))
    }

    fn piggybank(&self) -> FieldResult<Option<PiggyBank>> {
        Ok(
            read_model::PiggyBank::by_user_id(&self.user.as_ref()?.id, &get_connection())?
                .map(Into::into),
        )
    }
}
