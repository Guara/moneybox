pub mod login;
pub mod register;
use backend_lib::application::policies;
use backend_lib::application::policies::piggybank::CreatePiggyBankPolicy;
use backend_lib::read_model;
use bigdecimal::{BigDecimal, ParseBigDecimalError};
use juniper::{ParseScalarResult, ParseScalarValue, Value};
use serde::{Deserialize, Serialize};
use std::fmt::write;
use uuid::Uuid;

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct GqlDecimal(BigDecimal);

impl std::str::FromStr for GqlDecimal {
    type Err = ParseBigDecimalError;
    fn from_str(decimal: &str) -> Result<Self, Self::Err> {
        Ok(GqlDecimal(BigDecimal::from_str(decimal)?))
    }
}

impl std::fmt::Display for GqlDecimal {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write(f, format_args!("{}", self.0.to_string()))
    }
}

impl std::iter::Sum for GqlDecimal {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        Self(iter.fold(0.into(), |acc, b| acc + b.0))
    }
}

juniper::graphql_scalar!(GqlDecimal where Scalar = <S> {
    description: "Decimal number"

    // Define how to convert your custom scalar into a primitive type.
    resolve(&self) -> Value {
        Value::scalar(self.to_string())
    }

    // Define how to parse a primitive type into your custom scalar.
    from_input_value(v: &InputValue) -> Option<GqlDecimal> {
        v.as_scalar_value::<String>()
         .and_then(|s| s.parse().ok())
    }

    // Define how to parse a string value.
    from_str<'a>(value: ScalarToken<'a>) -> ParseScalarResult<'a, S> {
        <String as ParseScalarValue<S>>::from_str(value)
    }
});

#[derive(GraphQLInputObject, Debug, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ClientData {
    pub ip_address: String,
}

impl Into<policies::ClientData> for ClientData {
    fn into(self) -> policies::ClientData {
        policies::ClientData {
            ip_address: self.ip_address,
        }
    }
}

#[derive(GraphQLObject, Serialize, Debug)]
pub struct PiggyBank {
    pub id: Uuid,
    pub ibans: Option<Vec<String>>,
    pub credit_cards: Option<Vec<String>>,
    pub fund: GqlDecimal,
}

impl From<read_model::PiggyBank> for PiggyBank {
    fn from(piggybank: read_model::PiggyBank) -> Self {
        Self {
            id: piggybank.id,
            ibans: piggybank.ibans,
            credit_cards: piggybank.credit_cards,
            fund: GqlDecimal(piggybank.fund),
        }
    }
}

#[derive(GraphQLInputObject, Clone)]
pub struct NewPiggyBankData {
    pub piggybank: NewPiggyBank,
    pub client_data: ClientData,
}

impl Into<CreatePiggyBankPolicy> for NewPiggyBankData {
    fn into(self) -> CreatePiggyBankPolicy {
        CreatePiggyBankPolicy {
            new_piggybank: self.piggybank.into(),
            client_data: self.client_data.into(),
        }
    }
}

#[derive(GraphQLInputObject, Debug, Clone)]
pub struct NewPiggyBank {
    pub ibans: Option<Vec<String>>,
    pub credit_cards: Option<Vec<String>>,
    pub fund: GqlDecimal,
}

impl Into<policies::piggybank::NewPiggyBank> for NewPiggyBank {
    fn into(self) -> policies::piggybank::NewPiggyBank {
        policies::piggybank::NewPiggyBank {
            ibans: self.ibans,
            credit_cards: self.credit_cards,
            fund: self.fund.0,
        }
    }
}

#[derive(GraphQLObject, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct NewPiggyBankResponse {
    pub piggybank: PiggyBank,
}

impl From<read_model::PiggyBank> for NewPiggyBankResponse {
    fn from(piggybank: read_model::PiggyBank) -> Self {
        Self {
            piggybank: piggybank.into(),
        }
    }
}
