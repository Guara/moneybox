use crate::graphql::types::ClientData;
use backend_lib::application::policies::user::CreateUserPolicy;
use backend_lib::application::policies::{self};
use chrono::NaiveDate;
use serde::Serialize;

#[derive(GraphQLInputObject, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct RegisterData {
    pub password: String,
    pub email: String,
    pub primary_name: String,
    pub secondary_name: String,
    pub surname: String,
    pub birth_date: NaiveDate,
    pub address: String,
    pub client_data: ClientData,
}

impl Into<CreateUserPolicy> for RegisterData {
    fn into(self) -> CreateUserPolicy {
        CreateUserPolicy {
            client_data: self.client_data.clone().into(),
            new_user: self.into(),
        }
    }
}

impl Into<policies::user::NewUser> for RegisterData {
    fn into(self) -> policies::user::NewUser {
        policies::user::NewUser {
            password: self.password,
            email: self.email,
            primary_name: self.primary_name,
            secondary_name: self.secondary_name,
            surname: self.surname,
            birth_date: self.birth_date,
            address: self.address,
        }
    }
}

#[derive(GraphQLObject, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct RegisterResponse {
    pub success: bool,
}

impl RegisterResponse {
    pub fn ok() -> Self {
        Self { success: true }
    }
}
