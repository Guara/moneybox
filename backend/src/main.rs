mod graphql;

extern crate slog_term;
#[macro_use]
extern crate slog;
extern crate slog_async;
extern crate slog_stdlog;

#[macro_use]
extern crate juniper;

use crate::graphql::context::Context;
use crate::graphql::types::login::User;
use crate::graphql::{
    create_schema,
    schema::{MutationRoot, QueryRoot},
};
use backend_lib::crypto_utils::is_valid;
use backend_lib::db::connection::get_connection;
use backend_lib::read_model;
use juniper::RootNode;
use log::info;
use slog::Drain;
use std::env;
use std::net::SocketAddr;
use warp::Filter;

pub type Schema = RootNode<'static, QueryRoot, MutationRoot>;

const BACKEND_URL_ENV: &str = "BACKEND_URL";

fn main() {
    dotenv::dotenv().ok();
    let decorator = slog_term::TermDecorator::new().build();
    let drain = slog_term::FullFormat::new(decorator).build().fuse();
    let drain = slog_async::Async::new(drain).build().fuse();
    let logger = slog::Logger::root(drain, slog_o!("version" => env!("CARGO_PKG_VERSION")));
    let _scope_guard = slog_scope::set_global_logger(logger);
    slog_stdlog::init().unwrap();
    info!("standard logging redirected to slog");
    let log = warp::log("warp_server");
    let endpoint: SocketAddr = env::var(BACKEND_URL_ENV)
        .unwrap_or_else(|_| panic!("Environment variable {} must be set", BACKEND_URL_ENV))
        .parse()
        .unwrap_or_else(|_| {
            panic!(
                "Environment variable {} must be in SocketAddr format, e.g. 0.0.0.0:8000 or [2001:db8::1]:8080",
                BACKEND_URL_ENV
            )
        });
    info!("Listening on {}", endpoint);

    let context_extractor = warp::any()
        .and(warp::header::optional::<String>("authorization").map(
            |maybe_header: Option<String>| {
                maybe_header.map_or_else(Context::default, |auth_header| {
                    is_valid(&auth_header)
                        .ok()
                        .map_or_else(Context::default, |(_, user_id)| Context {
                            user: Some(User {
                                user: read_model::User::by_id(&user_id, &get_connection()),
                                permissions: vec![],
                            }),
                        })
                })
            },
        ))
        .boxed();
    let cors = warp::cors()
        .allow_any_origin()
        .allow_methods(vec!["GET", "POST"])
        .allow_headers(vec!["content-type"]);

    warp::serve(
        warp::get2()
            .and(warp::path("graphiql").and(juniper_warp::graphiql_filter("/graphql")))
            .or(warp::path("graphql").and(juniper_warp::make_graphql_filter(
                create_schema(),
                context_extractor,
            )))
            .with(log)
            .with(cors),
    )
    .run(endpoint);
}
