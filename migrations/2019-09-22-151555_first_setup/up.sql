CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE events
(
    id uuid NOT NULL DEFAULT uuid_generate_v4(),
    sequence_num bigserial NOT NULL,
    aggregate_id uuid NOT NULL,
    "name" varchar(255) NOT NULL,
    payload jsonb NOT NULL,
    inserted_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT (now() at time zone 'utc') NOT NULL,
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT (now() at time zone 'utc') NOT NULL,
    version INT NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (aggregate_id, version)
);


CREATE INDEX events_aggregate_id ON events(aggregate_id);
CREATE UNIQUE INDEX events_aggregate_id_version ON events(aggregate_id, version);

create TABLE users (
  id uuid PRIMARY KEY,
  email text UNIQUE NOT NULL,
  password text[] NOT NULL,
  primary_name text NOT NULL,
  secondary_name text NOT NULL,
  surname text NOT NULL,
  birth_date date NOT NULL,
  address text NOT NULL,
  registration_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT (now() at time zone 'utc') NOT NULL,
  ibans text[] NOT NULL DEFAULT array[]::text[],
  credit_cards text[] NOT NULL DEFAULT array[]::text[],
  fund numeric(17,2) NOT NULL DEFAULT 0 CHECK (fund >= 0), --Max storable fund: 999999999999999,99
  active boolean NOT NULL DEFAULT 'f'
);

create TABLE piggybanks (
  id uuid PRIMARY KEY,
  user_id uuid UNIQUE NOT NULL REFERENCES users (id),
  ibans text[],
  credit_cards text[],
  fund numeric(17,2) NOT NULL DEFAULT 0 CHECK (fund >= 0) --Max storable fund: 999999999999999,99
);


create TABLE boxes (
    id uuid PRIMARY KEY,
    piggybank_id uuid NOT NULL REFERENCES piggybanks (id),
    user_id uuid NOT NULL REFERENCES users (id),
    ibans text[],
    credit_cards text[],
    auto_deposit boolean NOT NULL DEFAULT 'f',
    auto_deposit_interval int NOT NULL DEFAULT 0,
    first_deposit uuid,
    last_deposit uuid,
    deposit_history uuid[], --intermediate only
    first_withdrawal uuid,
    last_withdrawal uuid,
    withdrawal_history uuid[], --intermediate only
    fund numeric(17,2) NOT NULL DEFAULT 0 CHECK (fund >= 0), --Max storable fund: 999999999999999,99
    goal uuid,
    shared boolean NOT NULL DEFAULT 'f',
    contributors text[],
    active boolean NOT NULL DEFAULT 'f'
);

create TABLE deposits (
    id uuid PRIMARY KEY,
    box_id uuid NOT NULL REFERENCES boxes (id),
    payment_type text[] NOT NULL,
    amount numeric(17,2) NOT NULL CHECK (amount > 0), --Max storable amount: 999999999999999,99
    deposit_date date NOT NULL,
    contributor uuid,
    is_automatic boolean NOT NULL
);

create TABLE withdrawals (
    id uuid PRIMARY KEY,
    box_id uuid NOT NULL REFERENCES boxes (id),
    user_id uuid NOT NULL,
    withdrawal_type text[] NOT NULL,
    amount numeric(17,2) NOT NULL CHECK (amount > 0), --Max storable amount: 999999999999999,99
    withdrawal_date date NOT NULL
);

create TABLE goals (
    id uuid PRIMARY KEY,
    box_id uuid NOT NULL REFERENCES boxes (id),
    description text NOT NULL,
    links text[],
    cost numeric(17,2) CHECK (cost > 0), --Max storable cost: 999999999999999,99
    updates text[] NOT NULL,
    is_automatic boolean NOT NULL,
    achieved boolean NOT NULL DEFAULT 'f'
);

alter table boxes add foreign key (first_deposit) REFERENCES deposits(id);
alter table boxes add foreign key (last_deposit) REFERENCES deposits(id);
--alter table boxes add foreign key (deposit_history) ELEMENT REFERENCES deposits(id);
alter table boxes add foreign key (first_withdrawal) REFERENCES withdrawals(id);
alter table boxes add foreign key (last_withdrawal) REFERENCES withdrawals(id);
--alter table boxes add foreign key (withdrawal_history) ELEMENT REFERENCES withdrawals(id);
alter table boxes add foreign key (goal) REFERENCES goals(id);
