use diesel::result::Error as DieselError;
use reqwest::Error as ReqwestError;
use serde_json::error::Error as SerdeError;
use std::io::Error as IOError;

pub type BackendResult<T> = Result<T, BackendError>;

#[derive(Debug, Fail)]
pub enum BackendError {
    #[fail(display = "Internal Server Error")]
    InternalServerError,

    #[fail(display = "BadRequest: {}", _0)]
    BadRequest(String),

    #[fail(display = "Unauthorized")]
    Unauthorized,

    #[fail(display = "User not found")]
    UserNotFound,

    #[fail(display = "User not active")]
    UserNotActive,

    #[fail(display = "Conflict: {}", _0)]
    Conflict(String),

    #[fail(display = "Generic error: {}", _0)]
    GenericError(String),

    #[fail(display = "Diesel error: {}", _0)]
    DieselError(DieselError),

    #[fail(display = "After inserting {} item diesel returns {}", _0, _1)]
    UnexpectedDatabaseInsertResult(usize, usize),

    #[fail(display = "Serde error: {}", _0)]
    SerdeError(SerdeError),

    #[fail(display = "Error generating a command: {}", _0)]
    CommandGeneration(String),

    #[fail(display = "Error performing an external request: {}", _0)]
    ExternalRequestErr(ReqwestError),

    #[fail(display = "Error during IO operation {}", _0)]
    IO(IOError),
}

impl From<ReqwestError> for BackendError {
    fn from(error: reqwest::Error) -> Self {
        BackendError::ExternalRequestErr(error)
    }
}

impl From<IOError> for BackendError {
    fn from(error: IOError) -> Self {
        BackendError::IO(error)
    }
}

impl From<SerdeError> for BackendError {
    fn from(error: SerdeError) -> Self {
        BackendError::SerdeError(error)
    }
}

impl From<DieselError> for BackendError {
    fn from(error: DieselError) -> Self {
        BackendError::DieselError(error)
    }
}
