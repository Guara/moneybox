use crate::models;
use chrono::NaiveDate;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Event {
    UserCreated {
        new_user: NewUser,
        client_data: ClientData,
    },
    LoginSuccessful {
        user_id: Uuid,
        client_data: ClientData,
    },
    LoginUnsuccessful {
        user_id: Uuid,
        client_data: ClientData,
    },
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct ClientData {
    pub ip_address: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NewUser {
    pub id: Uuid,
    pub password: Vec<String>,
    pub email: String,
    pub primary_name: String,
    pub secondary_name: String,
    pub surname: String,
    pub birth_date: NaiveDate,
    pub address: String,
}

impl Event {
    pub fn payload(&self) -> Value {
        serde_json::to_value(self).expect("evento non serializzabile")
    }

    pub fn aggregate_id(&self) -> Uuid {
        match self {
            Event::UserCreated { new_user, .. } => new_user.id,
            Event::LoginSuccessful { user_id, .. } => *user_id,
            Event::LoginUnsuccessful { user_id, .. } => *user_id,
        }
    }
}

impl ToString for Event {
    fn to_string(&self) -> String {
        match self {
            Event::UserCreated { .. } => "user_created",
            Event::LoginSuccessful { .. } => "login_successful",
            Event::LoginUnsuccessful { .. } => "login_unsuccessful",
        }
        .to_string()
    }
}

impl From<models::Event> for Event {
    fn from(event_model: models::Event) -> Self {
        serde_json::from_value(event_model.payload).unwrap()
    }
}
