use crate::db::connection::PgPooledConnection;
use crate::errors::BackendResult;
use crate::{domain, models};
use uuid::Uuid;

pub mod user;

pub type Version = u64;

/// trait dell'aggregates. Da usare per rappresentare in memory lo stato aggregato derivato dall'applicazione di una list di eventi temporalmente sequenziali
pub trait Aggregate {
    type Command;
    type State: AggregateState;

    fn initial_state() -> Self::State;
    fn apply_event(state: Self::State, event: &domain::Event) -> Self::State;
    fn handle_command(
        state: Option<&Self::State>,
        cmd: Self::Command,
    ) -> BackendResult<Vec<(Version, domain::Event)>> {
        let actual_version: u64 = state.map_or(0, |s| s.generation());
        let events = Self::do_handle_command(state, cmd)?;
        let events_with_versions =
            events
                .into_iter()
                .fold((actual_version, vec![]), |mut acc, e| {
                    acc.0 += 1;
                    acc.1.push((acc.0, e));
                    acc
                });
        Ok(events_with_versions.1)
    }

    fn do_handle_command(
        state: Option<&Self::State>,
        cmd: Self::Command,
    ) -> BackendResult<Vec<domain::Event>>;

    fn from_events(events: Vec<domain::Event>) -> Self::State {
        events.iter().fold(Self::initial_state(), |acc, event| {
            Self::apply_event(acc, event)
        })
    }

    fn apply_events(state: Self::State, events: Vec<domain::Event>) -> Self::State {
        events
            .iter()
            .fold(state, |acc, event| Self::apply_event(acc, event))
    }

    fn load(aggregate_id: Uuid, conn: &PgPooledConnection) -> Option<Self::State> {
        let events: Vec<domain::Event> = models::Event::by_aggregate_id(aggregate_id, conn)
            .into_iter()
            .map(|e| e.into())
            .collect();

        if events.is_empty() {
            None
        } else {
            Some(Self::apply_events(Self::initial_state(), events))
        }
    }
}

/// lo stato dell'aggregates
pub trait AggregateState {
    fn generation(&self) -> u64;
    fn id(&self) -> Uuid;
}

pub trait Projector {
    /// questa funzione proietta un evento in ogni read model che implementa il trait.
    /// Il result serve a intercettare gli errori generici.
    fn project(event: domain::Event, conn: &PgPooledConnection) -> BackendResult<()>;
}
