use crate::domain;
use crate::domain::aggregates::{Aggregate, AggregateState};
use crate::domain::{events, Event};
use crate::errors::{BackendError, BackendResult};
use uuid::Uuid;

#[derive(Debug)]
pub struct UserState {
    pub id: Uuid,
    generation: u64,
    active: bool,
}

impl Default for UserState {
    fn default() -> Self {
        Self {
            id: Uuid::new_v4(),
            generation: 0,
            active: false,
        }
    }
}

impl AggregateState for UserState {
    fn generation(&self) -> u64 {
        self.generation
    }

    fn id(&self) -> Uuid {
        self.id
    }
}

#[derive(Debug)]
pub enum UserCommand {
    Register {
        new_user: events::NewUser,
        client_data: events::ClientData,
    },
    Login {
        user_id: Uuid,
        client_data: events::ClientData,
    },
    LoginUnsuccessful {
        user_id: Uuid,
        client_data: events::ClientData,
    },
}

pub struct AggregateUser;

impl Aggregate for AggregateUser {
    type Command = UserCommand;
    type State = UserState;

    fn initial_state() -> Self::State {
        Default::default()
    }

    fn apply_event(state: Self::State, event: &domain::Event) -> Self::State {
        match event {
            _ => Self::State {
                generation: state.generation + 1,
                ..state
            },
        }
    }

    fn do_handle_command(
        state: Option<&Self::State>,
        cmd: Self::Command,
    ) -> BackendResult<Vec<domain::Event>> {
        match cmd {
            UserCommand::Register {
                new_user,
                client_data,
            } => {
                match state {
                    None => {
                        let event = Event::UserCreated {
                            new_user,
                            client_data,
                        };
                        Ok(vec![event])
                    }
                    Some(state) => {
                        let error_message = format!(
                            "Non è possibile registrare 2 volte uno user, user_id: {}",
                            state.id
                        );
                        //error!("{}", &error_message);
                        Err(BackendError::GenericError(error_message))
                    }
                }
            }
            UserCommand::Login {
                user_id,
                client_data,
            } => {
                match state {
                    None => {
                        let error_message = format!(
                            "Aggregate for user id: {} not exist, could not create a LoginSuccessful event",
                            user_id
                        );
                        //error!("{}", &error_message);
                        Err(BackendError::GenericError(error_message))
                    }
                    Some(_) => {
                        let event = Event::LoginSuccessful {
                            user_id,
                            client_data,
                        };
                        Ok(vec![event])
                    }
                }
            }
            UserCommand::LoginUnsuccessful {
                user_id,
                client_data,
            } => {
                match state {
                    None => {
                        let error_message = format!(
                            "Aggregate for user id: {} not exist, could not create a LoginUnsuccessful event",
                            user_id
                        );
                        //error!("{}", &error_message);
                        Err(BackendError::GenericError(error_message))
                    }
                    Some(_) => {
                        let event = Event::LoginUnsuccessful {
                            user_id,
                            client_data,
                        };
                        Ok(vec![event])
                    }
                }
            }
        }
    }
}
