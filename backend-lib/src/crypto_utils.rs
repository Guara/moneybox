use crate::errors::BackendError::GenericError;
use crate::errors::{BackendError, BackendResult};
use argon2rs::verifier::Encoded;
use chrono::{Duration, Utc};
use frank_jwt::{decode, encode, Algorithm, Error};
use lazy_static::lazy_static;
use log::{debug, error, info};
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use serde_json::json;
use serde_json::Value as JsonValue;
use std::iter;
use uuid::Uuid;

const STATIC_SALT: &str = "RrVxoqmqMxLOBCrb43L9V6o4KvG3rO2k";

lazy_static! {
    static ref MONEYBOX_ISS: String = "moneybox_api".into();
}

/// Returns the email and the user_id if `token` is a valid Token string.
pub fn is_valid(token: &str) -> BackendResult<(String, Uuid)> {
    let valid_token = token.replace("Bearer ", "");
    info!("token: {}", valid_token);
    let result: Result<(JsonValue, JsonValue), Error> = decode(
        &valid_token,
        &get_public(),
        Algorithm::ES512,
        //&ValidationOptions::default(),
    );
    match result {
        Ok(token) => Ok((
            token.1["sub"].to_string().replace("\"", ""),
            Uuid::parse_str(&token.1["uid"].to_string().replace("\"", ""))
                .map_err(|err| GenericError(err.to_string()))?,
        )),
        Err(e) => {
            info!("Not valid: {:?}", e);
            Err(BackendError::Unauthorized)
        }
    }
}

fn get_public() -> String {
    crate::EC_PUBLIC.to_string()
}

fn get_secret() -> String {
    crate::EC_PRIVATE.to_string()
}

pub fn hash_data(data: &str) -> Vec<String> {
    debug!("Start hashing data...");
    let random_salt: String = iter::repeat(())
        .map(|()| thread_rng().sample(Alphanumeric))
        .take(32)
        .collect();
    let random_hashed = hash_data_with_salt(data.as_bytes(), &*random_salt);
    debug!("First step hashed with random salt: {}", random_hashed);
    let static_hashed = hash_data_with_salt(random_hashed.as_bytes(), STATIC_SALT);
    debug!("Second step hashed with static salt: {}", static_hashed);
    [static_hashed, random_salt].to_vec()
}

fn hash_data_with_salt(data: &[u8], salt: &str) -> String {
    debug!("Hashing with salt: {}", salt);
    let encoded = Encoded::default2i(data, salt.as_bytes(), b"", b"");
    let data_hash = encoded.to_u8();
    match String::from_utf8(data_hash) {
        Ok(first_hash_string) => first_hash_string,
        Err(e) => panic!("Cannot parse first step encoded password, {:?}", e),
    }
}

pub fn compare_input_to_hashed_value(input: &str, stored_data: &[String]) -> bool {
    let stored_encoded = Encoded::from_u8(stored_data[0].as_bytes()).unwrap();
    let stored_random_salt = &stored_data[1];
    if stored_encoded.verify(hash_data_with_salt(input.as_bytes(), stored_random_salt).as_bytes()) {
        debug!("hash match!");
        true
    } else {
        debug!("hash doesn't match!");
        false
    }
}

pub fn create_token(email: &str, user_id: &Uuid, duration: Duration) -> BackendResult<String> {
    let payload = json!({
        "iss": MONEYBOX_ISS.to_string(),
        "sub": email.to_string(),
        "iat": Utc::now().to_rfc3339(),
        "exp": Utc::now().checked_add_signed(duration).unwrap().to_string(),
        "uid": user_id.to_string(),
    });
    let header = json!({
        "alg": "ES512",
        "typ": "jwt"
    });
    let jwt = encode(header, &get_secret(), &payload, Algorithm::ES512);
    match jwt {
        Ok(jwt) => {
            debug!("Token generated");
            Ok(jwt)
        }
        Err(e) => {
            error!(
                "Fail to create a new token for user {}, error: {}",
                email, e
            );
            Err(BackendError::InternalServerError)
        }
    }
}

pub fn create_refresh_token(email: &str, user_id: &Uuid) -> BackendResult<String> {
    debug!("Generating refresh token");
    create_token(email, user_id, Duration::days(30))
    // TODO store this on redis
}

pub fn create_auth_token(email: &str, user_id: &Uuid) -> BackendResult<String> {
    debug!("Generating auth token");
    create_token(email, user_id, Duration::minutes(10))
}

pub fn create_refresh_and_auth_token(
    email: &str,
    user_id: &Uuid,
) -> BackendResult<(String, String)> {
    debug!("Generating refresh and auth token");
    let refresh = create_refresh_token(email, user_id)?;
    let auth = create_auth_token(email, user_id)?;
    Ok((refresh, auth))
}
