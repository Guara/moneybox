use crate::db::connection::PgPooledConnection;
use crate::diesel::prelude::*;
use crate::domain::aggregates::Projector;
use crate::domain::aggregates::Version;
use crate::errors::{BackendError, BackendResult};
use crate::models;
use crate::{domain, read_model};

pub fn store_events(
    events: Vec<(Version, domain::Event)>,
    conn: &PgPooledConnection,
) -> BackendResult<Vec<models::Event>> {
    events
        .into_iter()
        .map(|event| store_event(event, conn))
        .collect()
}

pub fn store_event(
    event: (Version, domain::Event),
    conn: &PgPooledConnection,
) -> BackendResult<models::Event> {
    Ok(conn.transaction::<models::Event, BackendError, _>(|| {
        let event_model = models::Event::insert(
            models::NewEvent::new(
                event.1.aggregate_id(),
                event.1.to_string(),
                event.1.payload(),
                event.0 as i32,
            ),
            conn,
        )?;
        project_event(event.1, conn)?; //TODO separate project and event store
        Ok(event_model)
    })?)
}

pub fn project_event(event: domain::Event, conn: &PgPooledConnection) -> BackendResult<()> {
    Ok(read_model::User::project(event, conn)?)
}
