use crate::db::connection::PgPooledConnection;
use crate::db::pg_schema::events;
use crate::diesel::prelude::*;
use chrono::{DateTime, Utc};
use serde_json::Value;
use uuid::Uuid;

#[derive(Queryable, Identifiable, Debug)]
pub struct Event {
    pub id: Uuid,
    pub sequence_num: i64,
    pub aggregate_id: Uuid,
    pub name: String,
    pub payload: Value,
    pub inserted_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
    pub version: i32,
}

impl Event {
    pub fn insert(new_event: NewEvent, conn: &PgPooledConnection) -> QueryResult<Self> {
        diesel::insert_into(events::table)
            .values(&new_event)
            .get_result(conn)
    }

    pub fn by_aggregate_id(aggregate_id: Uuid, conn: &PgPooledConnection) -> Vec<Self> {
        events::table
            .filter(events::aggregate_id.eq(aggregate_id))
            .order(events::inserted_at.asc())
            .load::<Self>(conn)
            .expect("Error loading events")
    }
}

#[derive(Insertable, Debug)]
#[table_name = "events"]
pub struct NewEvent {
    pub id: Uuid,
    pub aggregate_id: Uuid,
    pub name: String,
    pub payload: Value,
    pub version: i32,
}

impl NewEvent {
    pub fn new(aggregate_id: Uuid, name: String, payload: Value, version: i32) -> Self {
        Self {
            id: Uuid::new_v4(),
            aggregate_id,
            name,
            payload,
            version,
        }
    }
}
