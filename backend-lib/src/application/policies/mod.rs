pub mod piggybank;
pub mod user;

use crate::db::connection::PgPooledConnection;
use crate::domain::events;
use crate::errors::BackendResult;

// the trait is limitating in returning useful data to the schema, but some sort of unification could be nice...
pub trait Policy {
    type Return;

    fn handle_input(self, conn: &PgPooledConnection) -> BackendResult<Self::Return>;
}

pub struct ClientData {
    pub ip_address: String,
}

impl Into<events::ClientData> for ClientData {
    fn into(self) -> events::ClientData {
        events::ClientData {
            ip_address: self.ip_address,
        }
    }
}
