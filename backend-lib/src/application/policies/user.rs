use crate::application::policies::{ClientData, Policy};
use crate::db::connection::PgPooledConnection;
use crate::domain::aggregates::user::{AggregateUser, UserCommand};
use crate::domain::aggregates::Aggregate;
use crate::domain::events;
use crate::errors::{BackendError, BackendResult};
use crate::event_sourcing::event_store;
use crate::{crypto_utils, read_model};
use chrono::NaiveDate;
use std::{thread, time};
use uuid::Uuid;

pub struct CreateUserPolicy {
    pub new_user: NewUser,
    pub client_data: ClientData,
}

pub struct LoginUserPolicy {
    pub login_data: LoginData,
    pub client_data: ClientData,
}

pub struct NewUser {
    pub password: String,
    pub email: String,
    pub primary_name: String,
    pub secondary_name: String,
    pub surname: String,
    pub birth_date: NaiveDate,
    pub address: String,
}

pub struct LoginData {
    pub email: String,
    pub password: String,
}

impl NewUser {
    fn password_strenght_check(&self) -> BackendResult<()> {
        Ok(())
    }
}

impl Into<events::NewUser> for NewUser {
    fn into(self) -> events::NewUser {
        events::NewUser {
            id: Uuid::new_v4(),
            password: crypto_utils::hash_data(self.password.as_str()),
            email: self.email,
            primary_name: self.primary_name,
            secondary_name: self.secondary_name,
            surname: self.surname,
            birth_date: self.birth_date,
            address: self.address,
        }
    }
}

impl Policy for CreateUserPolicy {
    type Return = ();

    fn handle_input(self, conn: &PgPooledConnection) -> BackendResult<()> {
        self.new_user.password_strenght_check()?;
        let new_user: events::NewUser = self.new_user.into();
        let client_data = self.client_data.into();
        let events_with_versions = AggregateUser::handle_command(
            AggregateUser::load(new_user.id, conn).as_ref(),
            UserCommand::Register {
                new_user,
                client_data,
            },
        )?;
        event_store::store_events(events_with_versions, conn)?;
        Ok(())
    }
}

impl Policy for LoginUserPolicy {
    type Return = (String, String); // TODO A struct should be returned instead of a tuple

    fn handle_input(self, conn: &PgPooledConnection) -> BackendResult<(String, String)> {
        let client_data = self.client_data.into();
        if let Some(user) = read_model::User::by_email(&self.login_data.email, conn)? {
            if user.active {
                do_login(user, self.login_data, client_data, conn)
            } else {
                Err(BackendError::UserNotActive)
            }
        } else {
            // TODO replace with real hash comparing to approx using the same time of execution
            thread::sleep(time::Duration::from_millis(40)); // Simulate password hash, because in this case the response is served 10 times faster and this could provide info on user enumeration
            Err(BackendError::UserNotFound) // Do not provide outside info on which field is wrong! but internally could be useful differentiate the error.
        } // We don't have an user_id to load the aggregate, so this invalid login attempt should be saved in other table maybe to implement some sort of blacklist
    }
}

fn do_login(
    user: read_model::User,
    login_data: LoginData,
    client_data: events::ClientData,
    conn: &PgPooledConnection,
) -> BackendResult<(String, String)> {
    if crypto_utils::compare_input_to_hashed_value(&login_data.password, &user.password) {
        let events_with_versions = AggregateUser::handle_command(
            AggregateUser::load(user.id, conn).as_ref(),
            UserCommand::Login {
                user_id: user.id,
                client_data,
            },
        )?;
        let (token, refresh_token) =
            crypto_utils::create_refresh_and_auth_token(&user.email, &user.id)?;
        event_store::store_events(events_with_versions, conn)?;
        Ok((token, refresh_token))
    } else {
        let events_with_versions = AggregateUser::handle_command(
            AggregateUser::load(user.id, conn).as_ref(),
            UserCommand::LoginUnsuccessful {
                user_id: user.id,
                client_data,
            },
        )?;
        event_store::store_events(events_with_versions, conn)?;
        Err(BackendError::Unauthorized) // After some wrong password login event the user should be locked!
    }
}
