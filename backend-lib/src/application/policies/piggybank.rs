use crate::application::policies::{ClientData, Policy};
use crate::db::connection::PgPooledConnection;
use crate::errors::BackendResult;
use crate::read_model::PiggyBank;
use bigdecimal::BigDecimal;

pub struct CreatePiggyBankPolicy {
    pub new_piggybank: NewPiggyBank,
    pub client_data: ClientData,
}

pub struct NewPiggyBank {
    pub ibans: Option<Vec<String>>,
    pub credit_cards: Option<Vec<String>>,
    pub fund: BigDecimal,
}

impl Policy for CreatePiggyBankPolicy {
    type Return = PiggyBank;

    fn handle_input(self, conn: &PgPooledConnection) -> BackendResult<Self::Return> {
        unimplemented!()
    }
}
