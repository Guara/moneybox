use crate::db::connection::PgPooledConnection;
use crate::db::pg_schema::users;
use crate::db::pg_schema::users::dsl::*;
use crate::domain;
use crate::domain::aggregates::Projector;
use crate::domain::events;
use crate::errors::BackendResult;
use bigdecimal::BigDecimal;
use chrono::{DateTime, NaiveDate, Utc};
use diesel::prelude::*;
use log::trace;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize, Identifiable, Queryable, Debug)]
#[table_name = "users"]
pub struct User {
    pub id: Uuid,
    pub email: String,
    pub password: Vec<String>,
    pub primary_name: String,
    pub secondary_name: String,
    pub surname: String,
    pub birth_date: NaiveDate,
    pub address: String,
    pub registration_date: DateTime<Utc>,
    pub ibans: Vec<String>,
    pub credit_cards: Vec<String>,
    pub fund: BigDecimal,
    pub active: bool,
}

impl User {
    pub fn by_id(user_id: &Uuid, conn: &PgPooledConnection) -> BackendResult<User> {
        use crate::db::pg_schema::users::dsl::*;
        trace!("Loading user by user_id {}", user_id);
        Ok(users.filter(id.eq(user_id)).first::<Self>(conn)?)
    }

    pub fn by_email(e_mail: &str, conn: &PgPooledConnection) -> BackendResult<Option<User>> {
        use crate::db::pg_schema::users::dsl::*;
        trace!("Loading user by email {}", e_mail);
        Ok(users
            .filter(email.eq(e_mail))
            .first::<Self>(conn)
            .optional()?)
    }
}

// NewUserData is used to extract data from a post request by the client
#[derive(Debug, Insertable)]
#[table_name = "users"]
pub struct NewUser {
    pub id: Uuid,
    pub password: Vec<String>,
    pub email: String,
    pub primary_name: String,
    pub secondary_name: String,
    pub surname: String,
    pub birth_date: NaiveDate,
    pub address: String,
}

impl NewUser {
    pub fn insert(self, conn: &PgPooledConnection) -> BackendResult<User> {
        diesel::insert_into(users)
            .values(self)
            .get_result::<User>(conn)
            .map_err(Into::into)
    }
}

impl From<events::NewUser> for NewUser {
    fn from(new_user: events::NewUser) -> Self {
        NewUser {
            id: new_user.id,
            password: new_user.password,
            email: new_user.email,
            primary_name: new_user.primary_name,
            secondary_name: new_user.secondary_name,
            surname: new_user.surname,
            birth_date: new_user.birth_date,
            address: new_user.address,
        }
    }
}

impl Projector for User {
    fn project(event: domain::Event, conn: &PgPooledConnection) -> BackendResult<()> {
        if let domain::Event::UserCreated { new_user, .. } = event {
            NewUser::from(new_user).insert(conn)?;
        }
        Ok(())
    }
}
