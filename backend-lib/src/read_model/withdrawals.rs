use crate::db::pg_schema::withdrawals;
use bigdecimal::BigDecimal;
use chrono::NaiveDate;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Identifiable, Serialize, Deserialize, Queryable)]
#[table_name = "withdrawals"]
pub struct Withdrawal {
    pub id: Uuid,
    pub box_id: Uuid,
    pub user_id: Uuid,
    pub withdrawal_type: Vec<String>,
    pub amount: BigDecimal,
    pub withdrawal_date: NaiveDate,
}
