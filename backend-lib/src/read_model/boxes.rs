use crate::db::pg_schema::boxes;
use bigdecimal::BigDecimal;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Identifiable, Serialize, Deserialize, Insertable, Queryable)]
#[table_name = "boxes"]
pub struct Box {
    pub id: Uuid,
    pub piggybank_id: Uuid,
    pub user_id: Uuid,
    pub ibans: Option<Vec<String>>,
    pub credit_cards: Option<Vec<String>>,
    pub auto_deposit: bool,
    pub auto_deposit_interval: i32,
    pub first_deposit: Option<Uuid>,
    pub last_deposit: Option<Uuid>,
    pub deposit_history: Option<Vec<Uuid>>,
    pub first_withdrawal: Option<Uuid>,
    pub last_withdrawal: Option<Uuid>,
    pub withdrawal_history: Option<Vec<Uuid>>,
    pub fund: BigDecimal,
    pub goal: Option<Uuid>,
    pub shared: bool,
    pub contributors: Option<Vec<String>>,
    pub active: bool,
}
