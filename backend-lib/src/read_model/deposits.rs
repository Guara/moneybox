use crate::db::pg_schema::deposits;
use bigdecimal::BigDecimal;
use chrono::NaiveDate;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Identifiable, Serialize, Deserialize, Queryable)]
#[table_name = "deposits"]
pub struct Deposit {
    pub id: Uuid,
    pub box_id: Uuid,
    pub payment_type: Vec<String>,
    pub amount: BigDecimal,
    pub deposit_date: NaiveDate,
    pub contributor: Option<Uuid>,
    pub is_automatic: bool,
}
