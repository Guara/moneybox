use crate::db::connection::PgPooledConnection;
use crate::db::pg_schema::piggybanks;
use crate::diesel::prelude::*;
use crate::errors::BackendResult;
use bigdecimal::BigDecimal;
use log::trace;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Identifiable, Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "piggybanks"]
pub struct PiggyBank {
    pub id: Uuid,
    pub user_id: Uuid,
    pub ibans: Option<Vec<String>>,
    pub credit_cards: Option<Vec<String>>,
    pub fund: BigDecimal,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NewPiggyBankWithoutUser {
    pub ibans: Option<Vec<String>>,
    pub credit_cards: Option<Vec<String>>,
    pub fund: BigDecimal,
}

impl NewPiggyBankWithoutUser {
    pub fn with_user(self, user_id: Uuid) -> PiggyBank {
        PiggyBank {
            id: Uuid::new_v4(),
            user_id,
            ibans: self.ibans,
            credit_cards: self.credit_cards,
            fund: self.fund,
        }
    }
}

impl PiggyBank {
    pub fn insert(self, conn: &PgPooledConnection) -> BackendResult<PiggyBank> {
        use crate::db::pg_schema::piggybanks::dsl::*;
        diesel::insert_into(piggybanks)
            .values(self)
            .get_result::<PiggyBank>(conn)
            .map_err(Into::into)
    }

    pub fn by_user_id(uid: &Uuid, conn: &PgPooledConnection) -> BackendResult<Option<Self>> {
        use crate::db::pg_schema::piggybanks::dsl::*;
        trace!("Loading piggybank by user_id {}", uid);
        Ok(piggybanks
            .filter(user_id.eq(uid))
            .first::<Self>(conn) //TODO return the vec
            .optional()?)
    }
}
