use crate::db::pg_schema::goals;
use bigdecimal::BigDecimal;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Identifiable, Serialize, Deserialize, Queryable)]
#[table_name = "goals"]
pub struct Goal {
    pub id: Uuid,
    pub box_id: Uuid,
    pub description: String,
    pub links: Option<Vec<String>>,
    pub cost: BigDecimal,
    pub updates: String,
    pub is_automatic: bool,
    pub achieved: bool,
}
