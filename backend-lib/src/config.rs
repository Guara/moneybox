use crate::errors::{BackendError, BackendResult};
use lazy_static::lazy_static;
use serde::Deserialize;
use slog::info;
use slog::Logger;
use slog_scope;
use std::fs;
use toml;

#[derive(Deserialize, Debug)]
pub struct Configuration {
    bind_address: String,
    log_path: String,
    ec_private_path: String,
    ec_public_path: String,
}

lazy_static! {
    static ref LOGGER: Logger = slog_scope::logger();
}

impl Configuration {
    pub fn load(pathstr: &str) -> BackendResult<Configuration> {
        println!("Loading config from {}", pathstr);
        let conf_file = fs::read_to_string(pathstr).expect("Fail to read configuration file");
        toml::from_str(&conf_file).map_err(|err| BackendError::GenericError(err.to_string()))
    }

    pub fn print_config(&self) {
        info!(LOGGER, "Config: {:?}", self)
    }

    pub fn get_bind_address(&self) -> &str {
        &self.bind_address
    }

    pub fn get_log_path(&self) -> &str {
        &self.log_path
    }

    pub fn get_ec_private_path(&self) -> &str {
        &self.ec_private_path
    }

    pub fn get_ec_public_path(&self) -> &str {
        &self.ec_public_path
    }
}
