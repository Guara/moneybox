pub mod application;
pub mod config;
pub mod crypto_utils;
pub mod db;
pub mod domain;
pub mod errors;
mod event_sourcing;
pub mod models;
pub mod read_model;

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate failure;

use config::Configuration;
use std::env;
use std::fs;

pub const SECRET_KEY: &str =
    "JKbYqkAGQSKZcgWv0hqOa1pcJk195Bo6HYz/l6hJq+RjtMNMteWstik0h759fXzgIyeMbJchQwcb4FsRqG7ufQ==";

lazy_static! {
    static ref CONFIG_PATH: String = {
        let args: Vec<String> = env::args().collect();
        if args.len() < 2 {
            panic!("Param -config CONFIG_PATH needed")
        }
        if args[1] == "-config" {
            args[2].clone()
        } else {
            panic!("First param -config CONFIG_PATH needed")
        }
    };
}

lazy_static! {
    static ref CONFIG: Configuration = {
        let conf: Configuration = match Configuration::load(&CONFIG_PATH) {
            Ok(_conf) => _conf,
            Err(err) => panic!("Fail to parse configuration file, {:?}", err),
        };
        conf
    };
}

lazy_static! {
    static ref BIND_ADDR: String = { CONFIG.get_bind_address().to_string() };
}

lazy_static! {
    static ref EC_PRIVATE: String =
        fs::read_to_string(env::var("EC_PRIVATE_PATH").expect("EC_PRIVATE_PATH must be set!"))
            .expect("Unable to read ec_private file");
}

lazy_static! {
    static ref EC_PUBLIC: String =
        fs::read_to_string(env::var("EC_PUBLIC_PATH").expect("EC_PUBLIC_PATH must be set!"))
            .expect("Unable to read ec_public file");
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
