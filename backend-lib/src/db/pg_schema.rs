table! {
    boxes (id) {
        id -> Uuid,
        piggybank_id -> Uuid,
        user_id -> Uuid,
        ibans -> Nullable<Array<Text>>,
        credit_cards -> Nullable<Array<Text>>,
        auto_deposit -> Bool,
        auto_deposit_interval -> Int4,
        first_deposit -> Nullable<Uuid>,
        last_deposit -> Nullable<Uuid>,
        deposit_history -> Nullable<Array<Uuid>>,
        first_withdrawal -> Nullable<Uuid>,
        last_withdrawal -> Nullable<Uuid>,
        withdrawal_history -> Nullable<Array<Uuid>>,
        fund -> Numeric,
        goal -> Nullable<Uuid>,
        shared -> Bool,
        contributors -> Nullable<Array<Text>>,
        active -> Bool,
    }
}

table! {
    deposits (id) {
        id -> Uuid,
        box_id -> Uuid,
        payment_type -> Array<Text>,
        amount -> Numeric,
        deposit_date -> Date,
        contributor -> Nullable<Uuid>,
        is_automatic -> Bool,
    }
}

table! {
    events (id) {
        id -> Uuid,
        sequence_num -> Int8,
        aggregate_id -> Uuid,
        name -> Varchar,
        payload -> Jsonb,
        inserted_at -> Timestamptz,
        updated_at -> Timestamptz,
        version -> Int4,
    }
}

table! {
    goals (id) {
        id -> Uuid,
        box_id -> Uuid,
        description -> Text,
        links -> Nullable<Array<Text>>,
        cost -> Nullable<Numeric>,
        updates -> Array<Text>,
        is_automatic -> Bool,
        achieved -> Bool,
    }
}

table! {
    piggybanks (id) {
        id -> Uuid,
        user_id -> Uuid,
        ibans -> Nullable<Array<Text>>,
        credit_cards -> Nullable<Array<Text>>,
        fund -> Numeric,
    }
}

table! {
    users (id) {
        id -> Uuid,
        email -> Text,
        password -> Array<Text>,
        primary_name -> Text,
        secondary_name -> Text,
        surname -> Text,
        birth_date -> Date,
        address -> Text,
        registration_date -> Timestamptz,
        ibans -> Array<Text>,
        credit_cards -> Array<Text>,
        fund -> Numeric,
        active -> Bool,
    }
}

table! {
    withdrawals (id) {
        id -> Uuid,
        box_id -> Uuid,
        user_id -> Uuid,
        withdrawal_type -> Array<Text>,
        amount -> Numeric,
        withdrawal_date -> Date,
    }
}

joinable!(boxes -> piggybanks (piggybank_id));
joinable!(boxes -> users (user_id));
joinable!(piggybanks -> users (user_id));

allow_tables_to_appear_in_same_query!(
    boxes,
    deposits,
    events,
    goals,
    piggybanks,
    users,
    withdrawals,
);
